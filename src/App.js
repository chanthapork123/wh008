
import React, { Component } from 'react'
import './App.css';
import DemoForm from './Component/DemoForm';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row ,Col,} from 'react-bootstrap';
import Account from './Component/Account';

export default class App extends Component {
  render() {
    return (
      <Container>
       <Row>
         <Col md={6}>
           <DemoForm/>
         </Col>
         <Col md={6}>
           <Account/>
         </Col>
       </Row>
     </Container>
    )
  }
}



          